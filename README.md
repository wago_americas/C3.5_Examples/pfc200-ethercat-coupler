# PFC200 + Ethercat coupler

## Project Details

- [ ] Written in Ladder
- [ ] Just Comms with an Ethercat coupler and its modules, and a counter
- [ ] Codesys Version 3.5.18.50
- [ ] Target: PFC200 750-8212 FW25


## Name
PFC + 354

## Description
Simple Comms PFC200 + Ethercat 354 coupler + IO's


![Project screenshots](PFC+354.jpg) 

## Support
This program is for demonstration only and does not include support.  May contain bugs.  Use at your own risk.

## Authors and acknowledgment
Sergio Vergara
May 5 2024

## License
Copyright (c) 2024, Sergio Vergara, WAGO

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 


```





